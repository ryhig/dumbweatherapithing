import pathlib
import argparse
import os
import socket
import logging
import pandas as pd

import parsingWeatherApi
import findLocationViaIP

HOSTNAME = 'one.one.one.one'
PORT = 80
TIMEOUT = 10

if os.name == 'nt':
    CONFIG_PATH = pathlib.Path('.test')
else:
    CONFIG_PATH = pathlib.Path.home() / '.config' / 'polybar'

PKL_PATH = CONFIG_PATH / 'weatherInfo.pkl'
LOG_PATH = CONFIG_PATH / 'weather.log'

COLUMNS_TO_PRINT = ['temperature', 'probabilityOfPrecipitation']

def valid_connection(hostname, logger) -> bool:
    logger = logging.getLogger('main.valid_connection')
    try:
        host = socket.gethostbyname(hostname)
        s = socket.create_connection((host, PORT), TIMEOUT)
        s.close()
        logger.info("Connection is valid.")
        return True
    except Exception:
        pass
    logger.error("Connection not found. :\\")
    return False 

def arg_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
            prog="dumbweatherapithing",
            description="Just a string to parse information from the national weather service's API")

    parser.add_argument('--gridpoints', type=str, help="gridpoint to use for api address")
    parser.add_argument('--office', type=str, help="office to use for api address")

    return parser

def setup_logging(logLocation) -> logging.Logger:

    logger = logging.getLogger('main')
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s: \n\t%(name)s \n\t%(levelname)s \n\t%(message)s')
    logFile = logging.FileHandler(logLocation)
    logFile.setLevel(logging.DEBUG)
    logFile.setFormatter(formatter)
    logger.addHandler(logFile)

    return logger

def main():
    parser = arg_parser()
    args = parser.parse_args()
    logger = setup_logging(LOG_PATH)
    df = None

    if args.gridpoints is None or args.office is None:
        api_address = findLocationViaIP.get_weather_forecast_hourly_address(TIMEOUT, logger)
        if api_address is None:
            logger.error(f'api_address is {api_address}... Attempting to pull local dataframe.')
        logger.info(f'using findLocationViaIP to set api_address')
    else:
        api_address = 'https://api.weather.gov/gridpoints/' + args.office + '/' \
            + args.gridpoints + '/forecast/hourly'
        logger.info(f'using gridpoints: {args.gridpoints} and office: {args.office} to set api_address')

    if api_address is not None:
        logger.info(f'api_address set to {api_address}')
        if valid_connection(HOSTNAME, logger):
            df = parsingWeatherApi.get_weather_data(PKL_PATH, TIMEOUT, api_address, logger)
        else:
            if PKL_PATH.exists():
                df = pd.read_pickle(PKL_PATH)
    else:
        if PKL_PATH.exists():
            df = pd.read_pickle(PKL_PATH)


    if df is not None:
        output = parsingWeatherApi.process_weather_data(df, PKL_PATH, COLUMNS_TO_PRINT, logger)
    else:
        logger.error("Invalid connection and weatherInfo.pkl does not exist...")
        output = "N/A"

    print(output)


if __name__ == '__main__':
    main()
