# dumbWeatherApiThing

Some quick and dirty implementation of pulling from the national weather service's api
and pulling temperature and weather for use with polybar modules.

## Things to do:
1. add logging... Some bugs happening still with N/A appearing. No tracebacks though since changing to pickle files.
2. expand information used from the weather api.
3. make code more modular or less strict? I'm not sure if that's necessary, 
but there is a lot of information that would need to be _hard coded_ for lack of a better phrase.
4. add polybar stuff? I'm not sure if that's within scope.
### Some thoughts
- I hate the idea of having some end user manually pip installing to their system python... 
I'm sure there is a way to source a venv for use with this script

