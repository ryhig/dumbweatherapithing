import logging
import datetime
import pandas as pd
import requests

DONT_CARE_COLUMNS = ['name', 'number', 'icon', 'temperatureTrend', \
        'temperatureUnit', 'shortForecast', 'detailedForecast']

def get_string_from_dataframe(parameter_list, df, logger) -> str:
    logger = logging.getLogger('main.parsingWeatherApi.createStringFromDataFrame')

    df = df[parameter_list]
    columns = df.columns
    return_string = ""
    date_extracted = False
    for i in columns:
        if i in DONT_CARE_COLUMNS:
            continue
        string_to_add = ""       
        item = df[i].item()
        if type(item) == dict:
            item = item['value']

        if type(item) == pd.Timestamp:
            if not date_extracted:
                string_to_add = f"{item.date()} "
                date_extracted = True
            string_to_add = string_to_add + f"H{item.time().hour} "
        elif i == 'isDaytime':
            if item:
                string_to_add = "DAY "
            else:
                string_to_add = "!DAY "
        elif i == 'temperature':
            string_to_add = f"{item}°F "
        elif i == 'probabilityOfPrecipitation':
            string_to_add = f"PoP:{item}% "
        elif i == 'dewpoint':
            string_to_add = "Dew:{item:.2f}% "
        elif i == 'relativeHumidity':
            string_to_add = f"H:{item}% "
        elif i == 'windSpeed':
            item = item.split(" ")
            string_to_add = item[0] + "MPH "
        elif i == 'windDirection':
            string_to_add = f"{item} "
        return_string = return_string + string_to_add 
    
    logger.info(f"Returning stringOf:\"{return_string.strip()}\"")
    return return_string.strip()

def process_weather_data(df, pkl_location, columns_to_print, logger):
    logger = logging.getLogger('main.parsingWeatherApi.processWeatherData')

    if df.empty:
        logging.error("DataFrame is empty...")
        return "N/A"

    df.to_pickle(pkl_location)
    df[['startTime', 'endTime']] = df[['startTime', 'endTime']].apply(pd.to_datetime)
    current_time = datetime.datetime.now(datetime.timezone.utc)
    if current_time >= df.iloc[df.index[-1]]['endTime']:
        logging.error("DataFrame time is out of bounds... Need to query WeatherAPI.")
        return "N/A"

    df = df[df.endTime >= current_time]
    df = df[df.startTime < current_time]
    
    logger.info("Reporting DataFrame information.")
    return get_string_from_dataframe(columns_to_print, df, logger)

def get_weather_data(pkl_location, timeout, api_address, logger) -> pd.DataFrame | None:
    df = None
    logger = logging.getLogger('main.parsingWeatherApi.get_weather_data')
    response = requests.get(api_address, headers={'Accept': 'application/geo+json'}, timeout=timeout)
    if response.status_code == 200:
        logger.info("Response status code is 200.")
        parsing_weather_api = response.json()
        df = pd.DataFrame.from_dict(parsing_weather_api['properties']['periods'])
    else:
        logger.error("Connection was valid, but request status code was not 200.")
        if pkl_location.exists():
            df = pd.read_pickle(pkl_location)

    return df

