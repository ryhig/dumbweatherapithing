import logging
import requests

DIGITS = 4

def get_ip(timeout, logger):
    logger = logging.getLogger('main.findLocationViaIP.get_ip')
    ip_address = 'https://api64.ipify.org?format=json'
    try:
        ip_response = requests.get(ip_address, timeout=timeout)
        if ip_response.status_code == 200:
            ip_response_json = ip_response.json()
        else:
            logger.error("Expected status code to be 200... Valid connection but could not reach {ip_address}")
            return None
    except Exception:
        logger.error(f'Could not connect to {ip_address}') 
        return None
    
    logger.info(f'Successful query to {ip_address}')
    return ip_response_json['ip']

def get_information_from_ip(ip_address, timeout, logger): 
    logger = logging.getLogger('main.findLocationViaIP.get_information_from_ip')
    ipapi_address = f'https://ipapi.co/{ip_address}/json'
    ipapi_response = requests.get(ipapi_address, timeout=timeout)
    if ipapi_response.status_code == 200:
        json = ipapi_response.json()
    else:
        logger.error(f'Expected status code to be 200... Valid connection but could not reach {ipapi_address}')
        return None, None
    logger.info(f'Successful query to {ipapi_address}')
    return json.get('latitude'), json.get('longitude')



def get_weather_forecast_hourly_address(timeout, logger):
    logger = logging.getLogger('main.findLocationViaIP.get_weather_forecast_hourly_address')
    ip_address = get_ip(timeout, logger)
    if ip_address is None:
        return None

    latitude, longitude = get_information_from_ip(ip_address, timeout, logger)
    logger.info(f'Latitude: {latitude}; Longitude: {longitude}')
    if latitude is None or longitude is None:
        return None

    weather_points_address = f'https://api.weather.gov/points/{latitude},{longitude}'
    weather_points_response = requests.get(weather_points_address, timeout=timeout)
    if weather_points_response.status_code == 200:
        weather_points_json = weather_points_response.json()
    else:
        logging.error(f'Expected status code to be 200... Valid connection but could not reach {weather_points_address}')
        return None
    
    weather_forecast_hourly = weather_points_json.get('properties')['forecastHourly']
    logger.info(f'Successful query to {weather_points_address}')
    return weather_forecast_hourly

if __name__ == "__main__":
    logger = logging.getLogger('main.findLocationViaIP')
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logLocation = 'test_findLocationViaIP.log'
    logFile = logging.FileHandler(logLocation)
    logFile.setLevel(logging.DEBUG)
    logFile.setFormatter(formatter)
    logger.addHandler(logFile)

    print(get_weather_forecast_hourly_address(2, logger))
