import os
import requests
from pathlib import Path
import tkinter as tk
from itertools import count
from PIL import ImageTk, Image

FILE_LOCATION = '/tmp/CONUS_loop.gif'
GIF_WEB_ADDRESS = 'https://radar.weather.gov/ridge/standard/CONUS_loop.gif'
MILLISECONDS = 400
NUMBER_OF_REPEATS = 3
POLYBAR_HEIGHT = 16
def update(index, frame_list, root, label, counter=0):
    frame = frame_list[index]
    index += 1
    if index == len(frame_list) - 1:
        counter += 1
        index = 0

    label.config(image=frame)
    if counter == NUMBER_OF_REPEATS:
        os._exit(status=0)
    else:
        root.after(MILLISECONDS, update, index, frame_list, root, label, counter)


def getCONUSweather():
    root = tk.Tk()
    label = tk.Label(root)
    label.pack()
    width, height = None, None
    window_width = root.winfo_screenwidth()
    frame_list = []

    if Path.is_file(Path(FILE_LOCATION)):
        Path(FILE_LOCATION).unlink()
    Path(FILE_LOCATION).touch()    
    with open(FILE_LOCATION, 'wb') as f:
        f.write(requests.get(GIF_WEB_ADDRESS).content)

    with Image.open(FILE_LOCATION, 'r') as gif:
        try:
            width, height = gif.size
            for i in count(1):
                frame_list.append(ImageTk.PhotoImage(gif.copy()))
                gif.seek(i)
        except EOFError:
            pass

    if width is not None and height is not None:
        x = window_width - width
        root.geometry(f'{width}x{height}+{x}+{POLYBAR_HEIGHT}')

    if Path.is_file(Path(FILE_LOCATION)):
        Path(FILE_LOCATION).unlink()

    root.after(0, update, 0, frame_list, root, label)
    root.mainloop()

if __name__ == '__main__':
    getCONUSweather()

